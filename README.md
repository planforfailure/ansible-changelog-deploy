# Changelog Deploy & Updater

A standardised changelog, similar to [motd](https://en.wikipedia.org/wiki/Motd_(Unix)) ensures each host is audited for all recent system changes when logging in via ssh.

### Components
- [changelog_initial_deploy.yml](changelog_initial_deploy.yml)
   - create `/var/log/CHANGELOG` on the host
   - copy CHANGELOG.sh to /etc/profile.d/
   - update `/var/log/CHANGELOG` with initial setup


- [update_changelog.yml](update_changelog.yml)
  - Static/Interactive vars set
  - Updates `/var/log/CHANGELOG` with defined variables set

### Summary
The _changelog_initial_deploy.yml_ is required to be deployed first to ensure all future changelog updates can run as expected.

`$ ansible-playbook initial_deploy.yml -bK`

Once this has successfuly completed with no failures, you can run _update_changelog.yml_  against all future updates.

`$ ansible-playbook update_changelog.yml -bK`
